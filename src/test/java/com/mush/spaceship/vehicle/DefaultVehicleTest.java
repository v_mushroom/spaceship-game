/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;

/**
 *
 * @author mush
 */
public class DefaultVehicleTest {

    public DefaultVehicleTest() {
    }

    /**
     * Test of createVehicle method, of class DefaultVehicle.
     */
    @Test
    public void testCreateVehicle() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        VehicleFactory vehicleFactory = new VehicleFactory(null);
        
        DefaultVehicle defaultVehicle = new DefaultVehicle(vehicleFactory);
        
        System.out.println(mapper.writeValueAsString(defaultVehicle.config));
        System.out.println(mapper.writeValueAsString(vehicleFactory.vehicleDesigns));
        System.out.println(mapper.writeValueAsString(vehicleFactory.engineDesigns));
    }

}
