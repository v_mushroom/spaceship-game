/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.ai;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.physics.FreeBody;
import com.mush.spaceship.vehicle.Vehicle;
import com.mush.spaceship.world.BulletEvent;
import com.mush.spaceship.world.VehicleEvent;
import com.mush.spaceship.world.World;

/**
 * AI for choosing navigation mode and possibly target
 *
 * @author mush
 */
public class VehicleCommander {

    public World world;
    public VehiclePilot pilot;

    private double totalSeconds = 0;
    private FreeBody wayPoint = new FreeBody(0);
    private double courage = 0;
    private Integer targetVehicleId;

    public VehicleCommander() {
        GameEventQueue.addListener(this);
    }

    public void destroy() {
        world = null;
        pilot = null;
        wayPoint = null;
        GameEventQueue.removeListener(this);
    }

    public void update(double elapsedSeconds) {
        totalSeconds += elapsedSeconds;
        courage += elapsedSeconds;
        if (courage > 5) {
            courage = 5;
        }
        if (courage > 0 && pilot.mode == VehiclePilot.Mode.FLEE) {
            pilot.mode = VehiclePilot.Mode.ATTACK;
        }

        if (totalSeconds > 3) {
            totalSeconds = 0;

            if (null != pilot.mode) {
                switch (pilot.mode) {
                    case NAVIGATE:
                        //randomWayPoint();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @OnGameEvent
    public void onBulletHit(BulletEvent.Hit event) {
        if (pilot != null && pilot.vehicle != null && event.hitVehicleId == pilot.vehicle.getId()) {
            onHitByBullet(event);
        }
    }

    @OnGameEvent
    public void onVehicleDestroyed(VehicleEvent.Destroy event) {
        if (targetVehicleId != null && targetVehicleId == event.vehicleId) {
            targetVehicleId = null;
            if (pilot != null) {
                pilot.targetBody = null;
            }
            wayPointTo(0, 0);
        }
    }

    public void onHitByBullet(BulletEvent.Hit event) {
        courage -= 1;
        if (courage < -5) {
            courage = -5;
        }
        if (courage < 0) {
            fleeFrom(event.ownerVehicleId);
        } else {
            if (pilot.mode != VehiclePilot.Mode.ATTACK) {
                attack(event.ownerVehicleId);
            }
        }

    }

    private void attack(int ownerVehicleId) {
        Vehicle vehicle = getVehicle(ownerVehicleId);
        if (vehicle != null) {
            targetVehicleId = ownerVehicleId;
            pilot.mode = VehiclePilot.Mode.ATTACK;
            pilot.targetBody = vehicle.freeBody;
        }
    }

    private void randomWayPoint() {
        wayPoint.xPosition = pilot.vehicle.freeBody.xPosition + (Math.random() - 0.5) * 500.0;
        wayPoint.yPosition = pilot.vehicle.freeBody.yPosition + (Math.random() - 0.5) * 500.0;
        pilot.targetBody = wayPoint;
    }

    private void wayPointTo(double x, double y) {
        wayPoint.xPosition = x;
        wayPoint.yPosition = y;
        pilot.targetBody = wayPoint;
        pilot.mode = VehiclePilot.Mode.NAVIGATE;
    }

    private void fleeFrom(int ownerVehicleId) {
        Vehicle vehicle = getVehicle(ownerVehicleId);
        if (vehicle != null) {
            targetVehicleId = ownerVehicleId;
            pilot.mode = VehiclePilot.Mode.FLEE;
            pilot.targetBody = vehicle.freeBody;
        }
    }

    private void idle() {
        if (pilot != null) {
            pilot.mode = VehiclePilot.Mode.IDLE;
        }
    }

    private Vehicle getVehicle(int vehicleId) {
        return world.vehicleIndex.get(vehicleId);
    }

}
