/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.ai;

import com.mush.game.utils.physics.FreeBody;
import com.mush.spaceship.vehicle.Vehicle;

/**
 * AI for navigating relative to a target
 *
 * @author mush
 */
public class VehiclePilot {

    public static enum Mode {
        IDLE,
        NAVIGATE,
        FOLLOW,
        ATTACK,
        RAM,
        FLEE
    }

    public Vehicle vehicle;
    public FreeBody targetBody;
    public double targetDistance;
    public double targetDistanceBuffer = 10;

    private double totalSeconds = 0;
    private double shiftStraight = 0;
    private double throttle = 0;
    private double turn = 0;
    public boolean fire = false;

    private VehiclePilotValues values = new VehiclePilotValues();

    public String state = "";
    public Mode mode = Mode.IDLE;

    public VehiclePilot() {
    }

    public void destroy() {
        vehicle = null;
        targetBody = null;
    }

    /**
     * Narrow cone
     *
     * @return
     */
    private boolean isTargetInNarrowFrontCone() {
        return values.absLocalTargetAngle <= Math.PI * 0.05;
    }

    /**
     * Wide cone
     *
     * @return
     */
    private boolean isTargetInFrontCone() {
        return values.absLocalTargetAngle < Math.PI * 0.025;
    }

    /**
     * Front half
     *
     * @return
     */
    private boolean isTargetInFront() {
        return values.absLocalTargetAngle < Math.PI * 0.5;
    }

    private boolean isTargetInRearQuarter() {
        return values.absLocalTargetAngle > Math.PI * 0.75;
    }

    private boolean isTargetInAttackRange() {
        return isTargetInNarrowFrontCone() && values.dist < targetDistance * 3;
    }

    private void measureDistance() {
        values.targetDx = targetBody.xPosition - vehicle.freeBody.xPosition;
        values.targetDy = targetBody.yPosition - vehicle.freeBody.yPosition;

        values.localTargetDx = vehicle.freeBody.getGlobalToLocalX(values.targetDx, values.targetDy);
        values.localTargetDy = vehicle.freeBody.getGlobalToLocalY(values.targetDx, values.targetDy);
        values.localTargetAngle = -Math.atan2(-values.localTargetDx, -values.localTargetDy);

        values.relLocalTargetXVel = 0;
        values.relLocalTargetYVel = 0;
        values.sideMovementAngVel = 0;

        values.updateDist();
        values.updateAbsAngle();
    }

    private void measureVelocities() {
        double relTargetXVel = targetBody.xVelocity - vehicle.freeBody.xVelocity;
        double relTargetYVel = targetBody.yVelocity - vehicle.freeBody.yVelocity;

        values.relLocalTargetXVel = vehicle.freeBody.getGlobalToLocalX(relTargetXVel, relTargetYVel);
        values.relLocalTargetYVel = vehicle.freeBody.getGlobalToLocalY(relTargetXVel, relTargetYVel);
        values.sideMovementAngVel = values.relLocalTargetXVel / values.dist;
    }

    private void measureTargetsPerspective() {
        double targetLocalDx = targetBody.getGlobalToLocalX(-values.targetDx, -values.targetDy);
        double targetLocalDy = targetBody.getGlobalToLocalY(-values.targetDx, -values.targetDy);
        values.targetLocalAngle = -Math.atan2(-targetLocalDx, -targetLocalDy);
        values.absTargetLocalAngle = Math.abs(values.targetLocalAngle);
    }

    private void compensateTargetAngleForRelativeMovement(double factor) {
        // Compensate target angle for relative sideways movement
        values.localTargetAngle += factor * values.sideMovementAngVel;
        values.updateAbsAngle();
    }

    private void turnTowardsTarget() {
        double maxTurningAngAcc = vehicle.freeBody.getAngAccForTorque(vehicle.maxTurningTorque);
        double angVel = vehicle.freeBody.zAngVelocity;

        if (!isTargetInFrontCone()) {
            double sAng = Math.signum(values.localTargetAngle);
            double sAngVel = Math.signum(angVel);

            if (sAng != sAngVel) {
                // Turning the wrong way
                turn = sAng;
            } else {
                // S,V
                // const A
                // ---
                // V = A * Tv
                // S = A * Ts^2 / 2
                // Tv = V / A
                // Ts = sqrt(2 * S / A)

                double aAngVel = Math.abs(angVel);
                double tV = aAngVel / maxTurningAngAcc;
                double tS = Math.sqrt(2 * values.absLocalTargetAngle / maxTurningAngAcc);
                if (tV > tS) {
                    // Going to stop
                    turn = 0;
                } else {
                    // Turn towards
                    turn = sAng;
                }
            }
        } else {
            turn = 0;
        }
    }

    private void turnAwayFromTarget() {
        double sAng = Math.signum(values.localTargetAngle);
        turn = 0;
        if (isTargetInFront()) {
            // turn away
            turn = -sAng;
        } else {
            if (isTargetInRearQuarter()) {
                // aiming at us?
                measureTargetsPerspective();
                if (values.absTargetLocalAngle < Math.PI * 0.1) {
                    double sTargetAng = Math.signum(values.targetLocalAngle);
                    turn = sTargetAng;
                }
            } else {
                // turn away
                //turn = -sAng;
            }
        }
    }

    private void leaveTarget() {
        if (isTargetInFront()) {
            brake();
        } else {
            if (values.dist < targetDistance * 2) {
                boost();
            } else {
                forward();
            }
        }
    }

    private void approachTarget() {
        state = "";

        if (isTargetInFront()) {
            // Target is in front
            if (mode == Mode.RAM) {
                approachTargetByRamming();
            } else {
                approachTargetToDistance();
            }
        } else {
            // Target is not in front of us
            forward();
            state += "not in front ";
        }
    }

    private void approachTargetByRamming() {
        double localVelY = -values.relLocalTargetYVel;
        if (localVelY > 0) {
            // We are moving away from target, backwards
            // We need to stop and go forward
            boost();
            state += "going backwards ";
        } else {
            if (values.dist > targetDistance) {
                state += "approach ";
                forward();
            } else {
                state += "ram ";
                boost();
            }
        }
    }

    private void approachTargetToDistance() {
        double dist = values.dist;
        if (dist > targetDistance) {
            // We need to get closer
            // Distance to perimeter
            dist -= targetDistance;
            double aDist = Math.abs(dist);
            // Not close enough, we need to approach
            double maxForwardAcc = vehicle.freeBody.getAccForForce(vehicle.maxForwardForce);
            double localVelY = -values.relLocalTargetYVel;

            if (localVelY > 0 && aDist > targetDistanceBuffer) {
                // We are moving away from target, backwards
                // We need to stop and go forward
                boost();
                state += "going backwards ";
            } else {
                // We are moving towards target, or not at all
                double aVel = -localVelY;
                double tV = aVel / maxForwardAcc;
                double tS = Math.sqrt(2 * dist / maxForwardAcc);
                if (tV > tS * 2) {
                    // Too fast, brake
                    brake();
                    state += "brake ";
                } else if (dist > targetDistance * 1.5) {
                    // Too far, need to get closer fast
                    boost();
                    state += "boost ";
                } else if (tV > tS) {
                    // Right on, stabilisation will stop us on target
                    idle();
                    state += "stopping ";
                } else if (aDist < targetDistanceBuffer) {
                    // Close enough to perimeter, go idle
                    idle();
                    state += "close ";
                } else {
                    // Too sloow, push forward
                    forward();
                    state += "approach ";
                }
            }
        } else {
            // We are too close, we need to back away
            brake();
            state += "too close ";
        }
    }

    public void update(double elapsedSeconds) {
        shiftStraight = 0;
        throttle = 0;
        turn = 0;
        fire = false;

        if (mode != Mode.IDLE && targetBody != null) {
            measureDistance();

            if (isTargetInFront()) {
                measureVelocities();
                compensateTargetAngleForRelativeMovement(1.0);
            }

            if (mode == Mode.ATTACK && isTargetInAttackRange()) {
                fire = true;
                compensateTargetAngleForRelativeMovement(-0.75);
            }

            if (mode == Mode.FLEE) {
                turnAwayFromTarget();

                leaveTarget();
            } else {
                turnTowardsTarget();

                approachTarget();
            }
        }

        vehicle.controls.shiftStraight = shiftStraight;
        vehicle.controls.shiftSideways = 0;
        vehicle.controls.throttle = throttle;
        vehicle.controls.turn = turn;
        vehicle.controls.fire = fire;
    }

    private void forward() {
        shiftStraight = 1;
        // check if vehicle has no shift engines,
        // in that case also use throttle
    }

    private void reverse() {
        shiftStraight = -1;
    }

    private void boost() {
        shiftStraight = 1;
        throttle = 1;
    }

    private void brake() {
        shiftStraight = -1;
        throttle = -1;
    }

    private void idle() {
        shiftStraight = 0;
        throttle = 0;
    }

}
