/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.ai;

/**
 *
 * @author mush
 */
public class VehiclePilotValues {

    double targetDx;
    double targetDy;

    double localTargetDx;
    double localTargetDy;
    double localTargetAngle;

    double relLocalTargetXVel;
    double relLocalTargetYVel;
    double sideMovementAngVel;

    double dist;
    double absLocalTargetAngle;
    
    double targetLocalAngle;
    double absTargetLocalAngle;

    public void updateDist() {
        dist = Math.sqrt(localTargetDx * localTargetDx + localTargetDy * localTargetDy);
    }

    public void updateAbsAngle() {
        absLocalTargetAngle = Math.abs(localTargetAngle);
    }
}
