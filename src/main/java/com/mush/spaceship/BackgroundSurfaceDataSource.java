/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteSheet;
import com.mush.game.utils.tiles.SurfaceDataSource;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class BackgroundSurfaceDataSource implements SurfaceDataSource {

    SpriteSheet spriteSheet;
    SpriteAnimation[] randomGround;

    public BackgroundSurfaceDataSource() {
        load();
    }

    private void load() {
        try {
            spriteSheet = new SpriteSheet("res/bg-stars.png", 64, 64);

            randomGround = new SpriteAnimation[4];
            randomGround[0] = makeAnimation(0, 0);
            randomGround[1] = makeAnimation(1, 0);
            randomGround[2] = makeAnimation(0, 1);
            randomGround[3] = makeAnimation(1, 1);

        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    private SpriteAnimation makeAnimation(int u, int v) {
        SpriteAnimation animation = new SpriteAnimation();

        animation.addFrame(spriteSheet.cutImage(u, v), 1.0);

        return animation;
    }

    @Override
    public SpriteAnimation getSurfaceData(int u, int v) {
        double r = Math.random();
        return randomGround[(int) (r * r * r * randomGround.length)];
    }
}
