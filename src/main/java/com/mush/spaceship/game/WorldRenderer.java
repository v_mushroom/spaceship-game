/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.game;

import com.mush.game.utils.physics.FreeBody;
import com.mush.game.utils.render.GameRenderer;
import com.mush.spaceship.vehicle.Vehicle;
import com.mush.spaceship.world.Bullet;
import com.mush.spaceship.world.Debris;
import com.mush.spaceship.world.Obstacle;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class WorldRenderer extends GameRenderer {

    public WorldContent content;

    public WorldRenderer(int width, int height) {
        super(width, height);
    }

    @Override
    public void renderContent(Graphics2D g) {

        AffineTransform t = g.getTransform();

        content.getWorld().backgroundSurface.draw(g);
        content.getWorld().surface.draw(g);

        // origin marker
        Point2D.Double p = content.getWorld().surface.getPosition();
        g.drawOval((int) p.x - 5, (int) p.y - 5, 10, 10);

        g.translate(-content.camera.getxOffset(), -content.camera.getyOffset());

        for (Obstacle obstacle : content.getWorld().obstacleList) {
            obstacle.draw(g);
        }

        for (Vehicle vehicle : content.getWorld().vehicleList) {
            drawVehicle(vehicle, g);
        }

        for (Bullet bullet : content.getWorld().bulletList) {
            bullet.draw(g);
        }

        for (Debris debris : content.getWorld().debrisList) {
            debris.draw(g);
        }

        g.setTransform(t);

        drawHud(content.getPlayerVehicle(), g);
    }

    private void drawHud(Vehicle playerVehicle, Graphics2D g) {
        if (playerVehicle != null) {
            g.setColor(playerVehicle.stabilization.limitTurning ? Color.GREEN : Color.RED);
            g.drawString("LIMIT", 50, 12);
            g.setColor(playerVehicle.stabilization.stabilizeTurning ? Color.GREEN : Color.RED);
            g.drawString("TURN", 100, 12);
            g.setColor(playerVehicle.stabilization.stabilizeSpeed ? Color.GREEN : Color.RED);
            g.drawString("SPEED", 150, 12);
            g.setColor(playerVehicle.stabilization.stabilizeDrift ? Color.GREEN : Color.RED);
            g.drawString("DRIFT", 200, 12);
        }
    }

    private void drawVehicle(Vehicle vehicle, Graphics2D g) {
        vehicle.draw(g);

        drawHealth(g, (int) vehicle.freeBody.xPosition, (int) (vehicle.freeBody.yPosition + vehicle.collisionRange), vehicle.hull);

//            drawSpeed(g, vehicle.freeBody);
        if (vehicle.pilot != null) {
            g.setColor(Color.BLACK);
            g.drawString(
                    vehicle.pilot.state,
                    (float) vehicle.freeBody.xPosition + 1,
                    (float) vehicle.freeBody.yPosition + 1 - 16);
            g.drawString(
                    vehicle.pilot.mode.name(),
                    (float) vehicle.freeBody.xPosition + 1,
                    (float) vehicle.freeBody.yPosition + 1 - 16 - 10);

            g.setColor(vehicle.pilot.fire ? Color.RED : Color.YELLOW);
            g.drawString(
                    vehicle.pilot.state,
                    (float) vehicle.freeBody.xPosition,
                    (float) vehicle.freeBody.yPosition - 16);

            g.setColor(Color.YELLOW);
            g.drawString(
                    vehicle.pilot.mode.name(),
                    (float) vehicle.freeBody.xPosition,
                    (float) vehicle.freeBody.yPosition - 16 - 10);
        }
    }

    private void drawHealth(Graphics2D g, int x, int y, double health) {
        int width = 20;
        int x0 = x - width / 2;
        int ofs = (int) (20 * health);
        g.setColor(Color.RED);
        g.drawLine(x0 + ofs, y, x0 + width, y);
        g.setColor(Color.GREEN);
        g.drawLine(x0, y, x0 + ofs, y);
    }

    private void drawSpeed(Graphics2D g, FreeBody body) {
        double speed = body.getSpeed();
        g.setColor(Color.BLACK);
        g.drawString("" + (int) (speed), (int) body.xPosition + 1, (int) body.yPosition + 1);

        g.setColor(Color.GREEN);
        g.drawString("" + (int) (speed), (int) body.xPosition, (int) body.yPosition);
    }

}
