/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.game;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.game.utils.ui.UiButton;
import com.mush.game.utils.ui.UiButtonList;
import com.mush.game.utils.ui.UiButtonRenderer;
import com.mush.spaceship.MainContent;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class HomeContent implements Updateable {

    private GameKeyListener keyListener;
    private GameKeyboard gameKeyboard;
    public UiButtonList buttonList;

    public HomeContent(GameKeyListener keyListener) {
        this.keyListener = keyListener;
        this.gameKeyboard = new GameKeyboard();

        gameKeyboard.bindActionKey(KeyEvent.VK_ENTER, KeyEvent.VK_ENTER);
        gameKeyboard.bindActionKey(KeyEvent.VK_UP, KeyEvent.VK_UP);
        gameKeyboard.bindActionKey(KeyEvent.VK_LEFT, KeyEvent.VK_UP);
        gameKeyboard.bindActionKey(KeyEvent.VK_DOWN, KeyEvent.VK_DOWN);
        gameKeyboard.bindActionKey(KeyEvent.VK_RIGHT, KeyEvent.VK_DOWN);

        keyListener.addKeyboard(gameKeyboard);

        GameEventQueue.addListener(this);

        buttonList = new UiButtonList();
        buttonList.location.x = 100;
        buttonList.location.y = 100;
        buttonList.loopSelection = true;
        buttonList.spacing = 10;
        UiButton button;

        button = new UiButton("Hello", new UiButtonRenderer());
        button.setSize(100, 20);
        buttonList.list.add(button);

        button = new UiButton("World", new UiButtonRenderer());
        button.setSize(70, 30);
        buttonList.list.add(button);

        button = new UiButton("Start", new UiButtonRenderer());
        button.setSize(90, 50);
        button.action = MainContent.NavigationEvent.SHOW_WORLD;
        buttonList.list.add(button);

        button = new UiButton("Exit", new UiButtonRenderer());
        button.setSize(30, 20);
        button.action = MainContent.NavigationEvent.QUIT;
        buttonList.list.add(button);

        buttonList.layout();
        buttonList.select(0);
    }

    public void clear() {
        keyListener.removeKeyboard(gameKeyboard);
        GameEventQueue.removeListener(this);
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            switch ((Integer) action.message) {
                case KeyEvent.VK_ENTER:
                    UiButton button = buttonList.getSelected();
                    if (button != null && button.action != null) {
                        GameEventQueue.send(button.action);
                    }
                    break;
                case KeyEvent.VK_UP:
                    buttonList.selectPrevious();
                    break;
                case KeyEvent.VK_DOWN:
                    buttonList.selectNext();
                    break;
            }
        }
    }

    @Override
    public void update(double elapsedSeconds) {
    }

    @Override
    public void updateCurrentFps(double fps) {

    }

}
