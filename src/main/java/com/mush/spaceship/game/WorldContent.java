/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.game;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.TrackingCamera;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.spaceship.MainContent;
import com.mush.spaceship.PlayerInput;
import com.mush.spaceship.PlayerInputAction;
import com.mush.spaceship.ai.VehicleCommander;
import com.mush.spaceship.ai.VehiclePilot;
import com.mush.spaceship.vehicle.Vehicle;
import com.mush.spaceship.world.World;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class WorldContent implements Updateable {

    PlayerInput playerInput = new PlayerInput();
    TrackingCamera camera;
    int playerVehicleId;

    private World world;

    private GameKeyListener keyListener;
    private GameKeyboard gameKeyboard;

    public WorldContent(GameKeyListener keyListener) throws IOException {
        this.keyListener = keyListener;
        this.gameKeyboard = new GameKeyboard();

        gameKeyboard.bindActionKey(KeyEvent.VK_ESCAPE, KeyEvent.VK_ESCAPE);
        gameKeyboard.bindActionKey(KeyEvent.VK_H, KeyEvent.VK_H);

        gameKeyboard.bindActionKey(KeyEvent.VK_1, KeyEvent.VK_1);
        gameKeyboard.bindActionKey(KeyEvent.VK_2, KeyEvent.VK_2);
        gameKeyboard.bindActionKey(KeyEvent.VK_3, KeyEvent.VK_3);
        gameKeyboard.bindActionKey(KeyEvent.VK_4, KeyEvent.VK_4);

        gameKeyboard.bindStateKey(KeyEvent.VK_A, PlayerInputAction.SHIFT_LEFT);
        gameKeyboard.bindStateKey(KeyEvent.VK_D, PlayerInputAction.SHIFT_RIGHT);
        gameKeyboard.bindStateKey(KeyEvent.VK_W, PlayerInputAction.SHIFT_FORWARD);
        gameKeyboard.bindStateKey(KeyEvent.VK_S, PlayerInputAction.SHIFT_BACKWARD);
        gameKeyboard.bindStateKey(KeyEvent.VK_Q, PlayerInputAction.TURN_LEFT);
        gameKeyboard.bindStateKey(KeyEvent.VK_E, PlayerInputAction.TURN_RIGHT);
        gameKeyboard.bindStateKey(KeyEvent.VK_SPACE, PlayerInputAction.THROTTLE);
        gameKeyboard.bindStateKey(KeyEvent.VK_X, PlayerInputAction.BRAKE);

        gameKeyboard.bindStateKey(KeyEvent.VK_LEFT, PlayerInputAction.TURN_LEFT);
        gameKeyboard.bindStateKey(KeyEvent.VK_RIGHT, PlayerInputAction.TURN_RIGHT);
        gameKeyboard.bindStateKey(KeyEvent.VK_UP, PlayerInputAction.SHIFT_FORWARD);
        gameKeyboard.bindStateKey(KeyEvent.VK_DOWN, PlayerInputAction.SHIFT_BACKWARD);

        gameKeyboard.bindStateKey(KeyEvent.VK_F, PlayerInputAction.FIRE);

        keyListener.addKeyboard(gameKeyboard);

        world = new World();

        camera = new TrackingCamera(
                MainContent.getContentWidth(), 
                MainContent.getContentHeight());
        camera.setSpeed(1);

        GameEventQueue.addListener(this);
        GameEventQueue.addListener(playerInput);

        world.addObstacle(400, -100, "POTATO_BIG", 1300, false);
        world.addObstacle(500, -100, "POTATO_MID", 13, false);
        world.addObstacle(500, -150, "POTATO_SMALL", 1.3, false);
//        world.addObstacle(510, -150, "POTATO_SMALL", 1.3, false);
//        world.addObstacle(520, -150, "POTATO_SMALL", 1.3, false);
//        world.addObstacle(530, -150, "POTATO_SMALL", 1.3, false);
//        world.addObstacle(540, -150, "POTATO_SMALL", 1.3, false);

        world.obstacleList.get(0).body.zRotation = 1;
    }

    public World getWorld() {
        return world;
    }

    public void clear() {
        world.clear();
        keyListener.removeKeyboard(gameKeyboard);
        GameEventQueue.removeListener(playerInput);
        GameEventQueue.removeListener(this);
    }

    private void createAiPilot(int vehicleId, int targetId) {
        VehiclePilot aiPilot = new VehiclePilot();
        aiPilot.vehicle = world.getVehicle(vehicleId);
        aiPilot.targetBody = world.getVehicle(targetId).freeBody;
        aiPilot.targetDistance = 100;
        aiPilot.vehicle.pilot = aiPilot;

        VehicleCommander commander = new VehicleCommander();
        commander.world = world;
        commander.pilot = aiPilot;
        aiPilot.vehicle.commander = commander;
    }

    public Vehicle getPlayerVehicle() {
        return world.getVehicle(playerVehicleId);
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            Vehicle playerVehicle = getPlayerVehicle();
            if (playerVehicle == null) {
                return;
            }
            switch ((Integer) action.message) {
                case KeyEvent.VK_ESCAPE:
                    GameEventQueue.send(MainContent.NavigationEvent.SHOW_HOME);
                    break;
                case KeyEvent.VK_H:
                    playerVehicle.freeBody.stop();
                    break;
                case KeyEvent.VK_1:
                    playerVehicle.stabilization.limitTurning = !playerVehicle.stabilization.limitTurning;
                    break;
                case KeyEvent.VK_2:
                    playerVehicle.stabilization.stabilizeTurning = !playerVehicle.stabilization.stabilizeTurning;
                    break;
                case KeyEvent.VK_3:
                    playerVehicle.stabilization.stabilizeSpeed = !playerVehicle.stabilization.stabilizeSpeed;
                    break;
                case KeyEvent.VK_4:
                    playerVehicle.stabilization.stabilizeDrift = !playerVehicle.stabilization.stabilizeDrift;
                    break;
            }
        }
    }

    @Override
    public void update(double elapsedSeconds) {
        Vehicle playerVehicle = getPlayerVehicle();

        if (playerVehicle != null) {
            playerInput.applyToVehicleControls(playerVehicle.controls);
        }

        keepNumberOfVehicles();

        world.update(elapsedSeconds);

        if (playerVehicle != null) {
            camera.setTarget(playerVehicle.freeBody.xPosition, playerVehicle.freeBody.yPosition);
        }

        camera.update(elapsedSeconds);

        world.updateSurfaces(camera.getxOffset(), camera.getyOffset(), elapsedSeconds);
    }

    private void keepNumberOfVehicles() {
        if (world.getVehicle(playerVehicleId) == null) {
            playerVehicleId = world.spawnVehicle("default", 100 + 200 * Math.random(), 100 + 200 * Math.random());
        }
        if (world.getVehicleCount() < 2) {
            int id = world.spawnVehicle("default", 100 + 200 * Math.random(), 100 + 200 * Math.random());
            createAiPilot(id, id);
            world.getVehicle(id).pilot.mode = VehiclePilot.Mode.IDLE;
        }
    }

    @Override
    public void updateCurrentFps(double fps) {

    }

}