/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.game;

import com.mush.game.utils.render.GameRenderer;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author mush
 */
public class HomeRenderer  extends GameRenderer {

    public HomeContent content;

    public HomeRenderer(int width, int height) {
        super(width, height);
    }

    @Override
    public void renderContent(Graphics2D g) {
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, getScreenWidth(), getScreenHeight());

        content.buttonList.draw(g);
    }

}
