/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.swing.Game;
import com.mush.game.utils.swing.GameKeyListener;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class Main {

    Game game;
    MainRenderer renderer;
    MainContent content;
    boolean fullScreen = false;

    public Main() {
        GameKeyListener keyListener = new GameKeyListener();
        try {
            content = new MainContent(keyListener);
        } catch (IOException e) {
            e.printStackTrace(System.out);
            return;
        }

        renderer = new MainRenderer(
                content.getContentWidth(), 
                content.getContentHeight());

        renderer.content = content;

        renderer.showFps(true);
        renderer.showFpsHistory(true);
        renderer.setPixelScale(1);

        setNormalRenderingHints();
        renderer.applyRenderingHints();

        game = new Game(content, renderer, keyListener);

        game.frameTitle = "Spaceship!";
        game.preferredSize.setSize(renderer.getScreenWidth(), renderer.getScreenHeight());
        game.setFrameUndecorated(true);
        game.setPauseOnLoseFocus(true);
        game.setUseActiveGraphics(true);
        game.start();

        game.getRefreshThread().setTargetFps(60);

        GameKeyboard gameKeyboard = new GameKeyboard();
        gameKeyboard.bindActionKey(KeyEvent.VK_F4, MainContent.NavigationEvent.QUIT);
        gameKeyboard.bindActionKey(KeyEvent.VK_HOME, KeyEvent.VK_HOME);

        keyListener.addKeyboard(gameKeyboard);
    }

    @OnGameEvent
    public void onAction(MainContent.NavigationEvent navigationEvent) {
        switch (navigationEvent) {
            case QUIT:
                game.close();
                break;
        }
    }
    
    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            switch ((Integer) action.message) {
                case KeyEvent.VK_HOME:
                    toggleFullScreen();
                    break;
            }
        }
        if (action.message instanceof MainContent.NavigationEvent) {
            switch ((MainContent.NavigationEvent)action.message) {
                case QUIT:
                    GameEventQueue.send(MainContent.NavigationEvent.QUIT);
                    break;
            }
        }
    }

    private void toggleFullScreen() {
        fullScreen = !fullScreen;
        if (fullScreen) {
            setFullscreenRenderingHints();
            Dimension display = game.getDisplaySize(null);
            double scale = (double) display.width / MainContent.getContentWidth();
            renderer.setPixelScale(scale);
            MainContent.resize(display.width, display.height);
            game.resize(display.width, display.height);
        } else {
            setNormalRenderingHints();
            renderer.setPixelScale(1);
            MainContent.resize(640, 360);
            game.resize(
                    MainContent.getContentWidth(), 
                    MainContent.getContentHeight());
        }
    }

    private void setNormalRenderingHints() {
        renderer.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        renderer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
    }

    private void setFullscreenRenderingHints() {
        renderer.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        renderer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    public static void main(String[] args) {
        Main main = new Main();
        GameEventQueue.addListener(main);
    }

}
