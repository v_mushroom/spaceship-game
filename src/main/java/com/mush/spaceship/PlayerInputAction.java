/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

/**
 *
 * @author mush
 */
public enum PlayerInputAction {
    THROTTLE,
    BRAKE,
    TURN_LEFT,
    TURN_RIGHT,
    SHIFT_LEFT,
    SHIFT_RIGHT,
    SHIFT_FORWARD,
    SHIFT_BACKWARD,
    LIFT_UP,
    LOWER_DOWN,
    FIRE
}
