/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

import com.mush.game.utils.render.GameRenderer;
import com.mush.spaceship.game.HomeRenderer;
import com.mush.spaceship.game.WorldRenderer;
import java.awt.Graphics2D;

/**
 *
 * @author mush
 */
public class MainRenderer extends GameRenderer {

    MainContent content;

    HomeRenderer homeRenderer;
    WorldRenderer worldRenderer;

    public MainRenderer(int width, int height) {
        super(width, height);
    }

    @Override
    public void renderContent(Graphics2D g) {
        if (content.worldContent != null) {
            if (worldRenderer == null) {
                worldRenderer = new WorldRenderer(getScreenWidth(), getScreenHeight());
                worldRenderer.content = content.worldContent;
            }

            worldRenderer.renderContent(g);
        } else {
            if (worldRenderer != null) {
                worldRenderer = null;
            }
        }
        if (content.homeContent != null) {
            if (homeRenderer == null) {
                homeRenderer = new HomeRenderer(getScreenWidth(), getScreenHeight());
                homeRenderer.content = content.homeContent;
            }

            homeRenderer.renderContent(g);
        } else {
            if (homeRenderer != null) {
                homeRenderer = null;
            }
        }
    }

}
