/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mush.game.utils.physics.EllipticalBodyEdge;
import com.mush.game.utils.sprites.Sprite;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.spaceship.vehicle.config.ComponentConfiguration;
import com.mush.spaceship.vehicle.config.EngineDesign;
import com.mush.spaceship.vehicle.config.GunDesign;
import com.mush.spaceship.vehicle.config.VehicleConfiguration;
import com.mush.spaceship.vehicle.config.VehicleDesign;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class VehicleFactory {

    public static class DesignConfiguration {

        public Map<String, VehicleDesign> vehicles;
        public Map<String, EngineDesign> engines;
        public Map<String, GunDesign> guns;
    }

    public static class VehicleConfigurationMap extends HashMap<String, VehicleConfiguration> {
    }

    private final SpriteFactory spriteFactory;
    public Map<String, VehicleDesign> vehicleDesigns;
    public Map<String, EngineDesign> engineDesigns;
    public Map<String, GunDesign> gunDesigns;
    public Map<String, VehicleConfiguration> vehicleConfigurations;

    public VehicleFactory(SpriteFactory spriteFactory) {
        this.spriteFactory = spriteFactory;
        vehicleDesigns = new HashMap<>();
        engineDesigns = new HashMap<>();
        gunDesigns = new HashMap<>();
        vehicleConfigurations = new HashMap<>();
    }

    public void loadDesigns(String filePath) throws IOException {
        System.out.println("Loading vehicle designs from " + filePath);
        File jsonFile = new File(filePath);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        DesignConfiguration configuration = mapper.readValue(jsonFile, DesignConfiguration.class);
        vehicleDesigns.putAll(configuration.vehicles);
        engineDesigns.putAll(configuration.engines);
        gunDesigns.putAll(configuration.guns);
    }

    public void loadVehicles(String filePath) throws IOException {
        System.out.println("Loading vehicle configs from " + filePath);
        File jsonFile = new File(filePath);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        VehicleConfigurationMap configuration = mapper.readValue(jsonFile, VehicleConfigurationMap.class);
        vehicleConfigurations.putAll(configuration);
    }

    public Vehicle createVehicle(VehicleConfiguration config) {
        VehicleDesign vehicleDesign = vehicleDesigns.get(config.designId);

        Vehicle vehicle = new Vehicle();
        vehicle.sprite = spriteFactory.createSprite(vehicleDesign.bodyShapeId);
        vehicle.sprite.setState(Vehicle.SPRITE_STATE);

        vehicle.baseMass = vehicleDesign.baseMass;
        vehicle.collisionRange = Math.max(vehicleDesign.width, vehicleDesign.length);
        vehicle.bodyEdge = new EllipticalBodyEdge(vehicleDesign.width / 2, vehicleDesign.length / 2);
        vehicle.angMassDistribution = vehicleDesign.angMassDistribution;
        vehicle.hullImpactFactor = vehicleDesign.impactFactor;

        vehicle.dragFactor = 0.0;

        for (Map.Entry<String, ComponentConfiguration> e : config.components.entrySet()) {
            String mountId = e.getKey();
            ComponentConfiguration compConfig = e.getValue();

            if (engineDesigns.containsKey(compConfig.designId)) {
                addEngine(createEngine(compConfig, vehicleDesign, mountId, spriteFactory), vehicle);
            } else if (gunDesigns.containsKey(compConfig.designId)) {
                addGun(createGun(compConfig, vehicleDesign, mountId, spriteFactory), vehicle);
            }
        }

        vehicle.recalculateValues();

        int index = 0;
        for (VehicleGun gun : vehicle.guns) {
            gun.gunId = index;
            index++;
        }

        return vehicle;
    }

    private void addEngine(VehicleEngine engine, Vehicle vehicle) {
        if (engine != null) {
            vehicle.engines.add(engine);
        }
    }

    private VehicleEngine createEngine(ComponentConfiguration config, VehicleDesign vehicleDesign, String mountId, SpriteFactory spriteFactory) {
        EngineDesign engineDesign = engineDesigns.get(config.designId);
        VehicleDesign.Mount mount = vehicleDesign.mounts.get(mountId);

        if (mount == null) {
            return null;
        }

        Sprite sprite = spriteFactory.createSprite(engineDesign.shapeId);
        sprite.setState(VehicleEngine.SPRITE_STATE_IDLE);

        VehicleEngine engine = new VehicleEngine(engineDesign.mass, mount.x, mount.y, sprite, config.engineRole, engineDesign.force);

        return engine;
    }

    private void addGun(VehicleGun gun, Vehicle vehicle) {
        if (gun != null) {
            vehicle.guns.add(gun);
        }
    }

    private VehicleGun createGun(ComponentConfiguration config, VehicleDesign vehicleDesign, String mountId, SpriteFactory spriteFactory) {
        GunDesign gunDesign = gunDesigns.get(config.designId);
        VehicleDesign.Mount mount = vehicleDesign.mounts.get(mountId);

        if (mount == null) {
            return null;
        }

        Sprite sprite = spriteFactory.createSprite("GUN_SMALL");
        sprite.setState(VehicleGun.SPRITE_STATE_IDLE);

        VehicleGun gun = new VehicleGun(gunDesign.mass, mount.x, mount.y, sprite, gunDesign.repeat, gunDesign.speed, gunDesign.impact, gunDesign.duration);

        return gun;
    }

}
