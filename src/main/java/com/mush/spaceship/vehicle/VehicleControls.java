/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

/**
 *
 * @author mush
 */
public class VehicleControls {

    public double throttle; // forward +1 .. -1 brake
    public double shiftStraight; // back -1 .. +1 front
    public double shiftSideways; // left -1 .. +1 right
    public double turn; // left -1 .. +1 right
    public double lift; // up +1..-1 down
    public boolean fire;

    public VehicleControls() {
        clear();
    }

    public void clear() {
        throttle = 0;
        shiftSideways = 0;
        shiftStraight = 0;
        turn = 0;
        lift = 0;
        fire = false;
    }

}
