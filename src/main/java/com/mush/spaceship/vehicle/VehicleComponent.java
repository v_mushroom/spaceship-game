/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.mush.game.utils.sprites.Sprite;

/**
 * Any component that can be placed on a vehicle's mount
 *
 * @author mush
 */
public class VehicleComponent {

    public Sprite sprite;
    public double mass;
    public int x;
    public int y;

    public VehicleComponent(double mass, int x, int y, Sprite sprite) {
        this.mass = mass;
        this.x = x;
        this.y = y;
        this.sprite = sprite;
    }

}
