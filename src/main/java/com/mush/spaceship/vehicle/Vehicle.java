/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.physics.BodyEdge;
import com.mush.game.utils.physics.FreeBody;
import com.mush.game.utils.sprites.Sprite;
import com.mush.spaceship.ai.VehicleCommander;
import com.mush.spaceship.ai.VehiclePilot;
import com.mush.spaceship.world.VehicleEvent;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class Vehicle {

    public static final String SPRITE_STATE = "default";

    // in-world entity id
    private int id;

    public FreeBody freeBody;
    public BodyEdge bodyEdge;
    public Sprite sprite;
    public VehiclePilot pilot;
    public VehicleCommander commander;
    public VehicleControls controls;
    public VehicleStabilization stabilization;
    public List<VehicleEngine> engines;
    public List<VehicleGun> guns;

    public double dragFactor = 0;
    public double baseMass = 1;
    public double angMassDistribution = 1;
    public double maxForwardForce;
    public double maxBackwardForce;
    public double maxRightwardForce;
    public double maxLeftwardForce;
    public double maxTurningTorque;
    public double maxThrottleForce;
    public double maxBrakeForce;

    public double receivedImpact = 0;
    public double collisionRange = 16;
    public double hull = 1;
    public double hullImpactFactor = 1;

    public Vehicle() {
        freeBody = new FreeBody(baseMass);
        bodyEdge = null; // new EllipticalBodyEdge(collisionRange, collisionRange);
        controls = new VehicleControls();
        stabilization = new VehicleStabilization(this);
        engines = new ArrayList<>();
        guns = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
        for (VehicleGun gun : guns) {
            gun.vehicleId = id;
        }
    }

    public int getId() {
        return id;
    }

    public void recalculateValues() {
        maxForwardForce = 0;
        maxBackwardForce = 0;
        maxLeftwardForce = 0;
        maxRightwardForce = 0;
        maxThrottleForce = 0;
        maxBrakeForce = 0;
        maxTurningTorque = 1;

        double mass = baseMass;

        for (VehicleEngine engine : engines) {
            mass += engine.mass;
            switch (engine.role) {
                case MAIN:
                    maxThrottleForce += engine.force;
                    break;
                case REVERSE:
                    maxBrakeForce += engine.force;
                    break;
                case BACKWARD:
                    maxBackwardForce += engine.force;
                    break;
                case FORWARD:
                    maxForwardForce += engine.force;
                    break;
                case RIGHTWARD:
                    maxRightwardForce += engine.force;
                    break;
                case LEFTWARD:
                    maxLeftwardForce += engine.force;
                    break;
            }
            if (engine.turnsLeft || engine.turnsRight) {
                maxTurningTorque += engine.force * 0.03;
            }
        }

        freeBody.setMass(mass, angMassDistribution);
        freeBody.setDragFactor(dragFactor);
    }

    public void update(double elapsedSeconds) {
        if (receivedImpact > 0) {
            hull -= hullImpactFactor * receivedImpact;
            if (hull <= 0) {
                GameEventQueue.send(new VehicleEvent.Destroy(this.id));
            }
        }

        if (commander != null) {
            commander.update(elapsedSeconds);
        }
        if (pilot != null) {
            pilot.update(elapsedSeconds);
        }

        stabilization.apply();

        double straightForce = controls.shiftStraight > 0
                ? controls.shiftStraight * maxForwardForce
                : controls.shiftStraight * maxBackwardForce;

        double sideForce = controls.shiftSideways > 0
                ? controls.shiftSideways * maxRightwardForce
                : controls.shiftSideways * maxLeftwardForce;

        double throttleForce = controls.throttle > 0
                ? controls.throttle * maxThrottleForce
                : controls.throttle * maxBrakeForce;

        double turnTorque = controls.turn * maxTurningTorque;

        for (VehicleEngine engine : engines) {
            switch (engine.role) {
                case MAIN:
                    engine.throttle = controls.throttle > 0 ? controls.throttle : 0;
                    break;
                case REVERSE:
                    engine.throttle = controls.throttle < 0 ? -controls.throttle : 0;
                    break;
                case RIGHTWARD:
                    engine.throttle = controls.shiftSideways < 0 ? -controls.shiftSideways : 0;
                    break;
                case LEFTWARD:
                    engine.throttle = controls.shiftSideways > 0 ? controls.shiftSideways : 0;
                    break;
                case BACKWARD:
                    engine.throttle = controls.shiftStraight < 0 ? -controls.shiftStraight : 0;
                    break;
                case FORWARD:
                    engine.throttle = controls.shiftStraight > 0 ? controls.shiftStraight : 0;
                    break;
            }
            if (controls.turn > 0 && engine.turnsRight) {
                engine.throttle += controls.turn;
            } else if (controls.turn < 0 && engine.turnsLeft) {
                engine.throttle += -controls.turn;
            }

            // move to engine.update();
            if (engine.sprite != null) {
                engine.sprite.setState(engine.throttle == 0
                        ? VehicleEngine.SPRITE_STATE_IDLE
                        : VehicleEngine.SPRITE_STATE_RUNNING);
                engine.sprite.update(elapsedSeconds);
            }
        }

        for (VehicleGun gun : guns) {
            gun.update(elapsedSeconds);
            if (controls.fire) {
                gun.fire();
            }
        }

        if (sprite != null) {
            sprite.update(elapsedSeconds);
        }

        freeBody.applyLocalForce(-sideForce, -straightForce);
        freeBody.applyLocalForce(0, -throttleForce);
        freeBody.applyTorque(turnTorque);

        freeBody.update(elapsedSeconds);

        freeBody.clearForces();

        receivedImpact = 0;
    }

    public void impact(double v) {
        receivedImpact += v;
    }

    public void draw(Graphics2D g) {
        AffineTransform t = g.getTransform();
        g.translate(freeBody.xPosition, freeBody.yPosition);
        g.rotate(freeBody.zRotation);

//        g.setColor(Color.GREEN);
//        Point2D.Double vel = new Point2D.Double();
//        freeBody.getLocalVelocity(vel);
//        g.drawLine(0, 0, 0, (int) (vel.y * 0.1));
//        for (int i = 0; i < 10; i++) {
//            g.drawLine(-10, -20 - i * 10, 10, -20 - i * 10);
//        }
        if (sprite != null) {
            sprite.draw(g, 0, 0);
        } else {
            g.setColor(Color.GREEN);
            g.drawRect(-16, -16, 32, 32);
            g.drawLine(0, -8, 0, -16);
        }
            g.setColor(Color.GREEN);
//            g.drawRect(-16, -16, 32, 32);
//            g.drawOval((int)-collisionRange/2, (int)-collisionRange, 2*(int)collisionRange/2, 2*(int)collisionRange);
            g.drawOval((int)-bodyEdge.getWidth()/2, (int)-bodyEdge.getHeight()/2, 2*(int)bodyEdge.getWidth()/2, 2*(int)bodyEdge.getHeight()/2);
            g.drawLine(0, -8, 0, -16);

        for (VehicleEngine engine : engines) {
            drawEngine(g, engine);
        }

        for (VehicleGun gun : guns) {
            drawGun(g, gun);
        }

        g.setTransform(t);
    }

    private void drawEngine(Graphics2D g, VehicleEngine engine) {
        if (engine.sprite != null) {
            drawEngineSprite(g, engine);
        } else {
            drawEngineOutline(g, engine);
        }
    }

    private void drawEngineSprite(Graphics2D g, VehicleEngine engine) {
        AffineTransform t = g.getTransform();
        g.translate(engine.x, engine.y);
        switch (engine.role) {
            case MAIN:
            case FORWARD:
                break;
            case REVERSE:
            case BACKWARD:
                g.rotate(Math.PI);
                break;
            case RIGHTWARD:
                g.rotate(Math.PI * 0.5);
                break;
            case LEFTWARD:
                g.rotate(Math.PI * 1.5);
                break;
        }
        engine.sprite.draw(g, 0, 0);
        g.setTransform(t);
    }

    private void drawEngineOutline(Graphics2D g, VehicleEngine engine) {
        g.setColor(engine.throttle > 0 ? Color.RED : Color.GREEN);
        g.drawRect(engine.x - 4, engine.y - 4, 8, 8);

        switch (engine.role) {
            case MAIN:
            case FORWARD:
                g.drawLine(engine.x, engine.y, engine.x + 0, engine.y + 10);
                break;
            case REVERSE:
            case BACKWARD:
                g.drawLine(engine.x, engine.y, engine.x + 0, engine.y - 10);
                break;
            case RIGHTWARD:
                g.drawLine(engine.x, engine.y, engine.x - 10, engine.y + 0);
                break;
            case LEFTWARD:
                g.drawLine(engine.x, engine.y, engine.x + 10, engine.y + 0);
                break;
        }
    }

    private void drawGun(Graphics2D g, VehicleGun gun) {
        AffineTransform t = g.getTransform();
        g.translate(gun.x, gun.y);
        gun.sprite.draw(g, 0, 0);
        g.setTransform(t);
    }

}
