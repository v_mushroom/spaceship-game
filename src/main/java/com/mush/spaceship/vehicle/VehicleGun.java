/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.sprites.Sprite;
import com.mush.spaceship.world.BulletEvent;

/**
 *
 * @author mush
 */
public class VehicleGun extends VehicleComponent {

    public static final String SPRITE_STATE_IDLE = "idle";
    public static final String SPRITE_STATE_FIRE = "fire";

    public int vehicleId;
    public int gunId;
    public boolean ready;
    public double timeSinceFiring;
    public final double repeatTime;
    public double bulletMass;

    public double bulletSpeed;
    public double bulletImpact;
    public double bulletDuration;

    public VehicleGun(double mass, int x, int y, Sprite sprite, double repeat, double speed, double impact, double duration) {
        super(mass, x, y, sprite);
        this.repeatTime = repeat;
        this.bulletSpeed = speed;
        this.bulletImpact = impact;
        this.bulletDuration = duration;
        this.bulletMass = 0.01;
    }

    public void update(double elapsedSeconds) {
        if (sprite != null) {
            sprite.update(elapsedSeconds);
        }
        if (ready) {
            return;
        }
        timeSinceFiring += elapsedSeconds;
        if (timeSinceFiring > repeatTime) {
            ready = true;
            if (sprite != null) {
                sprite.setState(SPRITE_STATE_IDLE);
            }
        }
    }

    public void fire() {
        if (!ready) {
            return;
        }
        ready = false;
        timeSinceFiring = 0;
        GameEventQueue.send(new BulletEvent.Fire(vehicleId, gunId));
        if (sprite != null) {
            sprite.setState(SPRITE_STATE_FIRE);
        }
    }

}
