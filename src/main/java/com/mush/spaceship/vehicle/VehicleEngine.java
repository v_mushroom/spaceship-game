/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.mush.game.utils.sprites.Sprite;

/**
 *
 * @author mush
 */
public class VehicleEngine extends VehicleComponent {

    public static final int TURN_TRESHOLD = 5;
    public static final String SPRITE_STATE_IDLE = "idle";
    public static final String SPRITE_STATE_RUNNING = "running";

    public static enum Role {
        MAIN,
        REVERSE,
        BACKWARD,
        FORWARD,
        LEFTWARD,
        RIGHTWARD
    }

    public Role role;
    public double force;
    public double throttle;
    public boolean turnsLeft;
    public boolean turnsRight;

    public VehicleEngine(double mass, int x, int y, Sprite sprite, Role role, double force) {
        super(mass, x, y, sprite);
        this.role = role;
        this.force = force;
        this.throttle = 0;
        this.turnsLeft = false;
        this.turnsRight = false;

        switch (role) {
            case MAIN:
            case FORWARD:
                turnsLeft = x > TURN_TRESHOLD;
                turnsRight = x < -TURN_TRESHOLD;
                break;
            case REVERSE:
            case BACKWARD:
                turnsLeft = x < -TURN_TRESHOLD;
                turnsRight = x > TURN_TRESHOLD;
                break;
            case RIGHTWARD:
                turnsLeft = y > TURN_TRESHOLD;
                turnsRight = y < -TURN_TRESHOLD;
                break;
            case LEFTWARD:
                turnsLeft = y < -TURN_TRESHOLD;
                turnsRight = y > TURN_TRESHOLD;
                break;
        }
    }
}
