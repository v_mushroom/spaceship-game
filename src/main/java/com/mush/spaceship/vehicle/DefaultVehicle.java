/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import com.mush.spaceship.vehicle.config.EngineDesign;
import com.mush.spaceship.vehicle.config.VehicleDesign;
import com.mush.spaceship.vehicle.config.VehicleDesign.Mount;
import com.mush.spaceship.vehicle.config.ComponentConfiguration;
import com.mush.spaceship.vehicle.config.VehicleComponentSize;
import com.mush.spaceship.vehicle.config.VehicleConfiguration;
import java.util.HashMap;

/**
 * Temporary, for testing configuration etc
 *
 * @author mush
 */
public class DefaultVehicle {

    public VehicleConfiguration config;

    public DefaultVehicle(VehicleFactory vehicleFactory) {
        this();

        VehicleDesign design = new VehicleDesign();
        design.baseMass = 1;
        design.angMassDistribution = 1;
        design.bodyShapeId = "SHIP";
        design.width = 32;
        design.length = 32;
        design.mounts = new HashMap<>();

        design.mounts.put("rear", mount(VehicleComponentSize.LARGE, 0, 15));
        design.mounts.put("front", mount(VehicleComponentSize.LARGE, 0, -15));

        design.mounts.put("front-left", mount(VehicleComponentSize.SMALL, -10, -10));
        design.mounts.put("front-right", mount(VehicleComponentSize.SMALL, 10, -10));
        design.mounts.put("rear-left", mount(VehicleComponentSize.SMALL, -10, 15));
        design.mounts.put("rear-right", mount(VehicleComponentSize.SMALL, 10, 15));

        design.mounts.put("left", mount(VehicleComponentSize.SMALL, -10, 7));
        design.mounts.put("right", mount(VehicleComponentSize.SMALL, 10, 7));

        vehicleFactory.vehicleDesigns.put("defaultShip", design);

        vehicleFactory.engineDesigns.put("engineSize4", engineDesign(1, 150, "ENGINE_BIG", VehicleComponentSize.LARGE));
        vehicleFactory.engineDesigns.put("engineSize3", engineDesign(0.5, 100, "ENGINE_BIG", VehicleComponentSize.LARGE));
        vehicleFactory.engineDesigns.put("engineSize2", engineDesign(0.1, 50, "ENGINE_SMALL", VehicleComponentSize.SMALL));
        vehicleFactory.engineDesigns.put("engineSize1", engineDesign(0.1, 30, "ENGINE_SMALL", VehicleComponentSize.SMALL));
    }

    public DefaultVehicle() {
        config = new VehicleConfiguration();
        config.designId = "defaultShip";
        config.components = new HashMap<>();

        config.components.put("rear", engineComponent("engineSize4", VehicleEngine.Role.MAIN));
        config.components.put("front", engineComponent("engineSize3", VehicleEngine.Role.REVERSE));

        config.components.put("front-left", engineComponent("engineSize1", VehicleEngine.Role.BACKWARD));
        config.components.put("front-right", engineComponent("engineSize1", VehicleEngine.Role.BACKWARD));
        config.components.put("rear-left", engineComponent("engineSize1", VehicleEngine.Role.FORWARD));
        config.components.put("rear-right", engineComponent("engineSize1", VehicleEngine.Role.FORWARD));

        config.components.put("left", engineComponent("engineSize2", VehicleEngine.Role.RIGHTWARD));
        config.components.put("right", engineComponent("engineSize2", VehicleEngine.Role.LEFTWARD));
    }

    private Mount mount(VehicleComponentSize size, int x, int y) {
        Mount mount = new Mount();
        mount.size = size;
        mount.x = x;
        mount.y = y;
        return mount;
    }

    private ComponentConfiguration engineComponent(String blueprint, VehicleEngine.Role engineRole) {
        ComponentConfiguration component = new ComponentConfiguration();

        component.designId = blueprint;
        component.engineRole = engineRole;

        return component;
    }

    private EngineDesign engineDesign(double mass, double force, String shapeId, VehicleComponentSize size) {
        EngineDesign design = new EngineDesign();
        design.mass = mass;
        design.force = force;
        design.shapeId = shapeId;
        design.size = size;
        return design;
    }

}
