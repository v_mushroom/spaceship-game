/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle;

import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class VehicleStabilization {

    public boolean stabilizeTurning = true;
    public boolean limitTurning = true;
    public boolean stabilizeDrift = false;
    public boolean stabilizeSpeed = false;

    private double maxTurningSpeed = Math.PI;
    private Vehicle vehicle;
    private boolean localVelocityValid;
    private Point2D.Double localVelocity = new Point2D.Double();

    public VehicleStabilization(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void apply() {
        limitTurning();
        if (stabilizeTurning) {
            stopTurning();
        }
        localVelocityValid = false;
        if (stabilizeDrift) {
            stopDrifting();
        }
        if (stabilizeSpeed) {
            stopSpeed();
        }
    }

    private void stopTurning() {
        if (vehicle.controls.turn == 0) {
            double aVel = Math.abs(vehicle.freeBody.zAngVelocity);
            if (aVel > 0.01) {
                vehicle.controls.turn = vehicle.freeBody.zAngVelocity > 0
                        ? -1
                        : 1;
            }
        }
    }

    private void limitTurning() {
        double turnVel = vehicle.freeBody.zAngVelocity;
        double aTurnVel = Math.abs(turnVel);
        double maxSpeed = limitTurning ? maxTurningSpeed : 5 * maxTurningSpeed;
        if (aTurnVel > maxSpeed) {
            vehicle.controls.turn = -Math.signum(turnVel);
        }
    }

    private void stopDrifting() {
        if (vehicle.controls.shiftSideways == 0) {
            if (!localVelocityValid) {
                vehicle.freeBody.getLocalVelocity(localVelocity);
                localVelocityValid = true;
            }
            double aVel = Math.abs(localVelocity.x);
            if (aVel > 1) {
                vehicle.controls.shiftSideways = localVelocity.x > 0
                        ? 1
                        : -1;
            }
        }
    }

    private void stopSpeed() {
        if (vehicle.controls.shiftStraight == 0 && vehicle.controls.throttle == 0) {
            if (!localVelocityValid) {
                vehicle.freeBody.getLocalVelocity(localVelocity);
                localVelocityValid = true;
            }
            double aVel = Math.abs(localVelocity.y);
            if (aVel > 1) {
                if (vehicle.controls.shiftStraight == 0) {
                    vehicle.controls.shiftStraight = localVelocity.y > 0
                            ? 1
                            : -1;
                }
                if (vehicle.controls.throttle == 0) {
                    vehicle.controls.throttle = localVelocity.y > 0
                            ? 1
                            : -1;
                }
            }
        }
    }

}
