/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle.config;

import java.util.Map;

/**
 *
 * @author mush
 */
public class VehicleDesign {

    public static class Mount {

        public int x;
        public int y;
        public VehicleComponentSize size;
    }

    public double baseMass;
    public double angMassDistribution;
    public String bodyShapeId;
    public int width;
    public int length;
    public double impactFactor;
    public Map<String, Mount> mounts;

}
