/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.vehicle.config;

import java.util.Map;

/**
 *
 * @author mush
 */
public class VehicleConfiguration {

    public String designId;
    public Map<String, ComponentConfiguration> components;
}
