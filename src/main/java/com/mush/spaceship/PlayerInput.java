/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.TwoInputValue;
import com.mush.spaceship.vehicle.VehicleControls;

/**
 *
 * @author mush
 */
public class PlayerInput {

    public TwoInputValue throttleBrake = new TwoInputValue();
    public TwoInputValue frontBack = new TwoInputValue();
    public TwoInputValue leftRight = new TwoInputValue();
    public TwoInputValue turnLeftRight = new TwoInputValue();
    public TwoInputValue liftLower = new TwoInputValue();
    public boolean fire;

    @OnGameEvent
    public void onAction(GameInputEvent.State state) {
        if (state.message instanceof PlayerInputAction) {
            switch ((PlayerInputAction) state.message) {
                case SHIFT_FORWARD:
                    frontBack.setInputA(state.active ? 1 : 0);
                    break;
                case SHIFT_BACKWARD:
                    frontBack.setInputB(state.active ? 1 : 0);
                    break;
                case SHIFT_LEFT:
                    leftRight.setInputA(state.active ? 1 : 0);
                    break;
                case SHIFT_RIGHT:
                    leftRight.setInputB(state.active ? 1 : 0);
                    break;
                case TURN_LEFT:
                    turnLeftRight.setInputB(state.active ? 1 : 0);
                    break;
                case TURN_RIGHT:
                    turnLeftRight.setInputA(state.active ? 1 : 0);
                    break;
                case THROTTLE:
                    throttleBrake.setInputA(state.active ? 1 : 0);
                    break;
                case BRAKE:
                    throttleBrake.setInputB(state.active ? 1 : 0);
                    break;
                case LIFT_UP:
                    liftLower.setInputA(state.active ? 1 : 0);
                    break;
                case LOWER_DOWN:
                    liftLower.setInputB(state.active ? 1 : 0);
                    break;
                case FIRE:
                    fire = state.active;
                    break;
            }
        }
    }

    public void applyToVehicleControls(VehicleControls controls) {
        controls.shiftStraight = frontBack.getOutput();
        controls.shiftSideways = leftRight.getOutput();
        controls.turn = turnLeftRight.getOutput();
        controls.throttle = throttleBrake.getOutput();
        controls.lift = liftLower.getOutput();
        controls.fire = fire;
    }

}
