/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

import com.mush.game.utils.map.MapBorder;
import com.mush.game.utils.map.MapTileComparator;
import com.mush.game.utils.sprites.SpriteAnimation;
import com.mush.game.utils.sprites.SpriteSheet;
import com.mush.game.utils.tiles.SurfaceDataSource;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author mush
 */
public class GroundDataSource implements SurfaceDataSource, MapTileComparator {

    SpriteSheet spriteSheet;
    SpriteAnimation[] randomGround;
    SpriteAnimation[] randomDust;
    SpriteAnimation[] border;

    public GroundDataSource() {
        load();
    }

    private void load() {
        try {
            spriteSheet = new SpriteSheet("res/ground2.png", 32, 32);

            randomGround = new SpriteAnimation[4];
            randomGround[0] = makeAnimation(0, 0);
            randomGround[1] = makeAnimation(1, 0);
            randomGround[2] = makeAnimation(2, 0);
            randomGround[3] = makeAnimation(3, 0);

            randomDust = new SpriteAnimation[3];
            randomDust[0] = makeAnimation(0, 4, 3, 0.5);
            randomDust[1] = makeAnimation(1, 4, 3, 0.5);
            randomDust[2] = makeAnimation(2, 4, 3, 0.5);

//            border = new SpriteAnimation[12];
/*            border[0] = makeAnimation(1, 1);
            border[1] = makeAnimation(1, 3);
            border[2] = makeAnimation(0, 2);
            border[3] = makeAnimation(2, 2);
             */
            border = makeBorder(1, 2);

        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    private SpriteAnimation[] makeBorder(int u0, int v0) {
        SpriteAnimation[] tiles = new SpriteAnimation[15];

        tiles[0] = makeAnimation(u0 + 0, v0 - 1);
        tiles[1] = makeAnimation(u0 + 1, v0 - 1);
        tiles[2] = makeAnimation(u0 + 1, v0 + 0);
        tiles[3] = makeAnimation(u0 + 1, v0 + 1);
        tiles[4] = makeAnimation(u0 + 0, v0 + 1);
        tiles[5] = makeAnimation(u0 - 1, v0 + 1);
        tiles[6] = makeAnimation(u0 - 1, v0 + 0);
        tiles[7] = makeAnimation(u0 - 1, v0 - 1);

        tiles[8] = makeAnimation(u0 + 2, v0 + 0);
        tiles[9] = makeAnimation(u0 + 2, v0 - 1);
        tiles[10] = makeAnimation(u0 + 3, v0 - 1);
        tiles[11] = makeAnimation(u0 + 3, v0 + 0);
        
        tiles[12] = makeAnimation(u0 + 2, v0 + 1);
        tiles[13] = makeAnimation(u0 + 3, v0 + 1);
        tiles[14] = makeAnimation(u0, v0);

        return tiles;
    }

//    private SpriteAnimation getBorderTile(SpriteAnimation[] tiles) {
//    }
    private SpriteAnimation makeAnimation(int u, int v) {
        SpriteAnimation animation = new SpriteAnimation();

        animation.addFrame(spriteSheet.cutImage(u, v), 1.0);

        return animation;
    }

    private SpriteAnimation makeAnimation(int u, int v, int steps, double time) {
        SpriteAnimation animation = new SpriteAnimation();

        for (int i = 0; i < steps; i++) {
            animation.addFrame(spriteSheet.cutImage(u, v + i), time);
        }

        return animation;
    }

    private Integer getSurfaceType(int u, int v) {
        /*
        if (u < 0 || u > 5 || v < 0 || v > 5) {
            return null;
        }
        */
        Integer n = null;
        
        Integer[][] values = new Integer[][]{
            new Integer[]{n, n, n, n, 1, 1},
            new Integer[]{n, 1, 1, 1, 1, 1},
            new Integer[]{1, 1, 1, 1, 1},
            new Integer[]{1, 1, 1, 1, n},
            new Integer[]{1, 1, 1, 1, 1, 1, n},
            new Integer[]{n, 1, 1, 1, 1, 1, 1},
            new Integer[]{n, n, n, n, n, 1, 1},
            new Integer[]{n, 1, 1, n, n},
            new Integer[]{n, 1, 1, n, 1}
        };
        if (u < 0 || v < 0) {
            return null;
        }
        if (v >= values.length) {
            return null;
        } 
        Integer[] line = values[v];
        if (u >= line.length) {
            return null;
        }
        return line[u];
    }

    @Override
    public boolean isSame(int u, int v, int uOfs, int vOfs) {
        Integer here = getSurfaceType(u, v);
        Integer there = getSurfaceType(u + uOfs, v + vOfs);
        return Objects.equals(here, there);
    }

    private MapBorder.Type getSurfaceBorderType(int u, int v) {
//        if (u < 0 || u > 5 || v < 0 || v > 5) {
//            return null;
//        }

        return MapBorder.getType(u, v, this);
//        return MapBorder.Type.BOTTOM;
    }

    @Override
    public SpriteAnimation getSurfaceData(int u, int v) {
        Integer type = getSurfaceType(u, v);
        if (type == null) {
            double r = Math.random();
            double f = 1.0 / (Math.sqrt(u * u + v * v));
            if (r * f < 0.1) {
                r = Math.random();
                if (r < 0.99) {
                    return null;
                }
            }
            r = Math.random();
            return randomDust[(int) (r * randomDust.length)];
        } else {
            MapBorder.Type borderType = getSurfaceBorderType(u, v);
            if (borderType == null) {
                double r = Math.random();
                return randomGround[(int) (r * r * r * randomGround.length)];
            } else {
                return getSurfaceData(borderType);
            }
        }
    }

    public SpriteAnimation getSurfaceData(MapBorder.Type borderType) {
        int i = borderType.ordinal();
        return i < border.length ? border[i] : null;
    }

    /*
    @Override
    public SpriteAnimation getSurfaceData(int u, int v) {
        double r = Math.random();

        if (u < 0 || u > 5 || v < 0 || v > 5) {
            double f = 1.0 / (Math.sqrt(u * u + v * v));
            if (r * f < 0.1) {
                r = Math.random();
                if (r < 0.99) {
                    return null;
                }
            }
            r = Math.random();
            return randomDust[(int) (r * randomDust.length)];
        }
        if (v == 0) {
            if (u == 0 || u == 5) {
                return null;
            }
            return border[0];
        } else if (v == 5) {
            if (u == 0 || u == 5) {
                return null;
            }
            return border[4];
        } else if (u == 0) {
            return border[6];
        } else if (u == 5) {
            return border[2];
        }
        return randomGround[(int) (r * r * r * randomGround.length)];
    }
     */
}
