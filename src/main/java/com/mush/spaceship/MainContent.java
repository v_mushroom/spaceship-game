/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.spaceship.game.HomeContent;
import com.mush.spaceship.game.WorldContent;
import java.io.IOException;

/**
 *
 * @author mush
 */
public class MainContent implements Updateable {

    public enum NavigationEvent {
        QUIT,
        SHOW_WORLD,
        SHOW_HOME
    }

    // Fixed
    private static final int contentWidth = 640;//960;
    // Relative to width and screen size, if fullscreen
    private static int contentHeight = 360;//540;

    WorldContent worldContent;
    HomeContent homeContent;

    GameKeyListener keyListener;

    public MainContent(GameKeyListener keyListener) throws IOException {
        this.keyListener = keyListener;

//        worldContent = new WorldContent(keyListener);
        homeContent = new HomeContent(keyListener);

        GameEventQueue.addListener(this);
    }
    
    public static void resize(int referenceWidth, int referenceHeight) {
        contentHeight = (contentWidth * referenceHeight) / referenceWidth;
    }
    
    public static int getContentWidth() {
        return contentWidth;
    }

    public static int getContentHeight() {
        return contentHeight;
    }
    
    @OnGameEvent
    public void onEvent(NavigationEvent event) {
        switch (event) {
            case SHOW_HOME:
                showHome();
                break;
            case SHOW_WORLD:
                showWorld();
                break;
        }
    }

    @Override
    public void update(double elapsedSeconds) {
        if (homeContent != null) {
            homeContent.update(elapsedSeconds);
        }
        if (worldContent != null) {
            worldContent.update(elapsedSeconds);
        }
        GameEventQueue.processQueue();
    }

    @Override
    public void updateCurrentFps(double fps) {

    }

    private void showHome() {
        if (worldContent != null) {
            worldContent.clear();
            worldContent = null;
        }
        if (homeContent == null) {
            homeContent = new HomeContent(keyListener);
        }
    }

    private void showWorld() {
        if (homeContent != null) {
            homeContent.clear();
            homeContent = null;
        }
        if (worldContent == null) {
            try {
                worldContent = new WorldContent(keyListener);
            } catch (IOException e) {
                e.printStackTrace();
                
            }
        }
    }

}
