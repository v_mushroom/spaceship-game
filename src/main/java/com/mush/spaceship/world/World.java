/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.world;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.physics.BodyCollision;
import com.mush.game.utils.physics.FreeBody;
import com.mush.game.utils.sprites.Sprite;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.game.utils.tiles.TiledSurface;
import com.mush.spaceship.BackgroundSurfaceDataSource;
import com.mush.spaceship.GroundDataSource;
import com.mush.spaceship.MainContent;
import com.mush.spaceship.vehicle.Vehicle;
import com.mush.spaceship.vehicle.VehicleFactory;
import com.mush.spaceship.vehicle.VehicleGun;
import com.mush.spaceship.vehicle.config.VehicleConfiguration;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mush
 */
public class World {

    public GroundDataSource groundDataSource;
    public BackgroundSurfaceDataSource backgroundDataSource;
    public TiledSurface surface;
    public TiledSurface backgroundSurface;
    public List<Vehicle> vehicleList;
    public Map<Integer, Vehicle> vehicleIndex;
    public List<Bullet> bulletList;
    public List<Debris> debrisList;
    public List<Obstacle> obstacleList;

    private int nextVehicleId = 1;
    private VehicleFactory vehicleFactory;
    private SpriteFactory spriteFactory;
    private double purgeBulletsTimeCounter = 0;
    private double purgeBulletsTimePeriod = 1.0;
    private int vehicleCount = 0;

    public World() throws IOException {
        spriteFactory = new SpriteFactory();
        vehicleFactory = new VehicleFactory(spriteFactory);

        spriteFactory.load("res/sprites.json");
        vehicleFactory.loadDesigns("res/designs.json");
        vehicleFactory.loadVehicles("res/ships.json");

        groundDataSource = new GroundDataSource();
        backgroundDataSource = new BackgroundSurfaceDataSource();

        surface = new TiledSurface(
                MainContent.getContentWidth(), 
                MainContent.getContentHeight(), 
                32, 32);
        surface.setDataSource(groundDataSource);

        backgroundSurface = new TiledSurface(
                MainContent.getContentWidth(), 
                MainContent.getContentHeight(), 
                64, 64);
        backgroundSurface.setDataSource(backgroundDataSource);

        vehicleList = new ArrayList<>();
        vehicleIndex = new HashMap<>();
        bulletList = new ArrayList<>();
        debrisList = new ArrayList<>();
        obstacleList = new ArrayList<>();

        GameEventQueue.addListener(this);
    }

    public void clear() {
        GameEventQueue.removeListener(this);
    }

    public Vehicle getVehicle(int vehicleId) {
        return vehicleIndex.get(vehicleId);
    }

    public int getVehicleCount() {
        return vehicleCount;
    }

    public int spawnVehicle(String configId, double x, double y) {
        VehicleConfiguration config = vehicleFactory.vehicleConfigurations.get(configId);
        Vehicle vehicle = vehicleFactory.createVehicle(config);
        vehicle.freeBody.xPosition = x;
        vehicle.freeBody.yPosition = y;

        addVehicle(vehicle);

        return vehicle.getId();
    }

    public void unspawnVehicle(int vehicleId) {
        Vehicle vehicle = removeVehicle(vehicleId);
        if (vehicle.pilot != null) {
            vehicle.pilot.destroy();
        }
        if (vehicle.commander != null) {
            vehicle.commander.destroy();
        }
        vehicle.pilot = null;
        vehicle.commander = null;
    }

    public void addVehicle(Vehicle vehicle) {
        vehicle.setId(nextVehicleId);
        nextVehicleId++;
        vehicleList.add(vehicle);
        vehicleIndex.put(vehicle.getId(), vehicle);
        vehicleCount++;
    }

    public Vehicle removeVehicle(int vehicleId) {
        Vehicle vehicle = vehicleIndex.get(vehicleId);
        if (vehicle != null) {
            vehicleList.remove(vehicle);
            vehicleIndex.remove(vehicleId);
            vehicleCount--;
        }
        return vehicle;
    }

    public Debris addDebris(double xPosition, double yPosition, String spriteType, Double duration) {
        Debris debris = new Debris(xPosition, yPosition);
        debris.sprite = spriteFactory.createSprite(spriteType);
        debris.sprite.setState("default");
        BufferedImage image = debris.sprite.getImage();
        Double debrisDuration = duration != null
                ? duration
                : debris.sprite.getAnimationDuration();
        debris.duration = debrisDuration != null ? debrisDuration : 0;
        debrisList.add(debris);
        return debris;
    }

    public Obstacle addObstacle(double xPosition, double yPosition, String spriteType, double mass, boolean fixed) {
        Sprite sprite = spriteFactory.createSprite(spriteType);
        double radius = 10;
        if (sprite != null) {
            sprite.setState("default");
            BufferedImage image = sprite.getImage();
            if (image != null) {
                radius = image.getWidth() / 2;
            }
        }
        Obstacle obstacle = new Obstacle(xPosition, yPosition, sprite, radius);
        obstacle.body.setMass(mass, 1);
        obstacle.fixed = fixed;
        obstacleList.add(obstacle);
        return obstacle;
    }

    @OnGameEvent
    public void onFireBullet(BulletEvent.Fire event) {
//        System.out.println("fire bullet event, vehicle:" + event.vehicleId);
        Vehicle vehicle = vehicleIndex.get(event.ownerVehicleId);
        if (vehicle != null) {
            VehicleGun gun = vehicle.guns.get(event.gunId);
            if (gun != null) {
                Bullet bullet = new Bullet(vehicle.freeBody, gun);
                bullet.ownerId = vehicle.getId();
                bullet.fire(gun.bulletSpeed, gun.bulletImpact, gun.bulletDuration);
                bulletList.add(bullet);
            }
        }
    }

    @OnGameEvent
    public void onVehicleDestroyed(VehicleEvent.Destroy event) {
        Vehicle vehicle = vehicleIndex.get(event.vehicleId);
        if (vehicle != null) {
            System.out.println("destroy " + vehicle.getId());
            Debris debris = addDebris(vehicle.freeBody.xPosition, vehicle.freeBody.yPosition, "EXPLOSION", null);
            debris.scale = 5;
            unspawnVehicle(vehicle.getId());
        }
    }

    public void update(double elapsedSeconds) {
        purgeBulletsTimeCounter += elapsedSeconds;
        if (purgeBulletsTimeCounter >= purgeBulletsTimePeriod) {
            purgeBulletsTimeCounter = 0;
            purgeExpiredBullets();
            purgeExpiredDebris();
        }
        for (Vehicle vehicle : vehicleList) {
            vehicle.update(elapsedSeconds);
//            attract(vehicle.freeBody);
        }
        for (Bullet bullet : bulletList) {
            bullet.update(elapsedSeconds);
//            attract(bullet.body);
        }
        for (Debris debris : debrisList) {
            debris.update(elapsedSeconds);
        }
        for (Obstacle obstacle : obstacleList) {
            obstacle.update(elapsedSeconds);
//            attract(obstacle.body);
        }
        checkBulletCollisions();
        checkObstacleCollisions();
        checkVehicleCollisions();
    }

    public void updateSurfaces(double cameraX, double cameraY, double elapsedSeconds) {
        Point2D.Double surfacePosition = surface.getPosition();
        surface.move(-cameraX - surfacePosition.x, -cameraY - surfacePosition.y);
        surface.update(elapsedSeconds);

        surfacePosition = backgroundSurface.getPosition();
        backgroundSurface.move(-cameraX * 0.51 - surfacePosition.x, -cameraY * 0.51 - surfacePosition.y);
        backgroundSurface.update(elapsedSeconds);
    }

    private void purgeExpiredBullets() {
        List<Bullet> purgedList = new ArrayList<>();
        for (Bullet bullet : bulletList) {
            if (!bullet.expired) {
                purgedList.add(bullet);
            }
        }
        bulletList.clear();
        bulletList = purgedList;
//        System.out.println("purged, new size: " + bulletList.size());
    }

    private void purgeExpiredDebris() {
        List<Debris> purgedList = new ArrayList<>();
        for (Debris debris : debrisList) {
            if (!debris.expired) {
                purgedList.add(debris);
            }
        }
        debrisList.clear();
        debrisList = purgedList;
//        System.out.println("purged, new size: " + bulletList.size());
    }

    private void checkBulletCollisions() {
        for (Bullet bullet : bulletList) {
            if (!bullet.expired) {
                for (Vehicle vehicle : vehicleList) {
                    if (vehicle.getId() != bullet.ownerId) {
                        double range = vehicle.collisionRange;
                        if (BodyCollision.isCoarseCollision(bullet.body, vehicle.freeBody, range)) {
                            double speed = BodyCollision.collide(bullet.body, vehicle.freeBody, null, vehicle.bodyEdge, 1);
                            if (speed > 0) {
                                bullet.expired = true;
                                // fire event
                                addDebris(bullet.body.xPosition, bullet.body.yPosition, "EXPLOSION", null);

                                GameEventQueue.send(new BulletEvent.Hit(bullet.ownerId, vehicle.getId()));

                                vehicle.impact(bullet.getDamage());
                            }
                        }
                    }
                }
                for (Obstacle obstacle : obstacleList) {
                    double range = obstacle.radius;
                    if (BodyCollision.isCoarseCollision(bullet.body, obstacle.body, range)) {
                        double speed = BodyCollision.collide(bullet.body, obstacle.body, null, obstacle.bodyEdge, 1);
                        if (speed > 0) {
                            bullet.expired = true;
                            // fire event
                            addDebris(bullet.body.xPosition, bullet.body.yPosition, "EXPLOSION", null);
                        }
                    }
                }
            }
        }
    }

    private void checkVehicleCollisions() {
        // find a good value
        double factor = 0.01;
        for (int i = 0; i < vehicleList.size(); i++) {
            Vehicle vehicleA = vehicleList.get(i);
            for (int j = i + 1; j < vehicleList.size(); j++) {
                Vehicle vehicleB = vehicleList.get(j);
                double range = vehicleA.collisionRange + vehicleB.collisionRange;

                if (BodyCollision.isCoarseCollision(vehicleA.freeBody, vehicleB.freeBody, range)) {
                    double speed = BodyCollision.collide(vehicleA.freeBody, vehicleB.freeBody, vehicleA.bodyEdge, vehicleB.bodyEdge, 0.75);
                    if (speed > 0) {
                        speed *= factor;
                        vehicleA.impact(speed);
                        vehicleB.impact(speed);
                    }
                }
            }
        }
    }

    private void checkObstacleCollisions() {
        for (int i = 0; i < obstacleList.size(); i++) {
            Obstacle obstacleA = obstacleList.get(i);
            for (int j = i + 1; j < obstacleList.size(); j++) {
                Obstacle obstacleB = obstacleList.get(j);
                double range = obstacleA.radius + obstacleB.radius;

                if (BodyCollision.isCoarseCollision(obstacleA.body, obstacleB.body, range)) {
                    BodyCollision.collide(obstacleA.body, obstacleB.body, obstacleA.bodyEdge, obstacleB.bodyEdge, 0.75);
                }
            }
        }
        // find a good value
        double factor = 0.01;
        for (int i = 0; i < vehicleList.size(); i++) {
            Vehicle vehicle = vehicleList.get(i);
            for (int j = 0; j < obstacleList.size(); j++) {
                Obstacle obstacle = obstacleList.get(j);
                double range = vehicle.collisionRange + obstacle.radius;

                if (BodyCollision.isCoarseCollision(vehicle.freeBody, obstacle.body, range)) {
                    double speed = BodyCollision.collide(vehicle.freeBody, obstacle.body, vehicle.bodyEdge, obstacle.bodyEdge, 0.75);
                    if (speed > 0) {
                        speed *= factor;
                        vehicle.impact(speed);
                    }
                }
            }
        }
    }

    /**/
    private void attract(FreeBody body) {
        double strength = 100;
        Obstacle center = obstacleList.get(0);
        double x = center.body.xPosition;
        double y = center.body.yPosition;
        double radius = 200;

        double dx = x - body.xPosition;
        double dy = y - body.yPosition;
        double dist = Math.sqrt(dx * dx + dy * dy);
        if (dist < 10) {
            return;
        }
        dx /= dist;
        dy /= dist;

        double force = strength / dist;
        body.xVelocity += dx * force;
        body.yVelocity += dy * force;

    }
    /**/

}
