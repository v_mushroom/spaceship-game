/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.world;

import com.mush.game.utils.physics.FreeBody;
import com.mush.game.utils.sprites.Sprite;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author mush
 */
public class Debris {

    public FreeBody body;
    public double duration;
    public boolean expired;
    public Sprite sprite;
    public double scale;

    public Debris(double x, double y) {
        body = new FreeBody(1);
        body.xPosition = x;
        body.yPosition = y;
        expired = false;
        scale = 1;
    }

    public void update(double elapsedSeconds) {
        if (expired) {
            return;
        }
        duration -= elapsedSeconds;
        if (duration <= 0) {
            expired = true;
        } else {
            body.update(elapsedSeconds);
            if (sprite != null) {
                sprite.update(elapsedSeconds);
            }
        }
    }

    public void draw(Graphics2D g) {
        if (expired) {
            return;
        }

        if (sprite != null) {
            AffineTransform t = g.getTransform();
            g.translate(body.xPosition, body.yPosition);
            g.scale(scale, scale);
            sprite.draw(g, 0, 0);
            g.setTransform(t);
        }
    }

}
