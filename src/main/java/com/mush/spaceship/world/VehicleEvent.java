/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.world;

/**
 *
 * @author mush
 */
public abstract class VehicleEvent {

    public final int vehicleId;

    public VehicleEvent(int id) {
        this.vehicleId = id;
    }

    public static class Destroy extends VehicleEvent {

        public Destroy(int vehicleId) {
            super(vehicleId);
        }
    }

}
