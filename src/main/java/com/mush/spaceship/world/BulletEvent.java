/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.world;

/**
 *
 * @author mush
 */
public abstract class BulletEvent {

    public final int ownerVehicleId;

    public BulletEvent(int ownerId) {
        this.ownerVehicleId = ownerId;
    }

    public static class Fire extends BulletEvent {
        
        public final int gunId;

        public Fire(int vehicleId, int gunId) {
            super(vehicleId);
            this.gunId = gunId;
        }
    }

    public static class Hit extends BulletEvent {

        public final int hitVehicleId;

        public Hit(int ownerId, int hitId) {
            super(ownerId);
            hitVehicleId = hitId;
        }
    }

}
