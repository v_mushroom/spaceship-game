/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.world;

import com.mush.game.utils.physics.BodyEdge;
import com.mush.game.utils.physics.CircularBodyEdge;
import com.mush.game.utils.physics.FreeBody;
import com.mush.game.utils.sprites.Sprite;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author mush
 */
public class Obstacle {

    public FreeBody body;
    public BodyEdge bodyEdge;
    public Sprite sprite;
    public double radius;
    public boolean fixed;

    public Obstacle(double x, double y, Sprite sprite, double radius) {
        body = new FreeBody(1);
        body.xPosition = x;
        body.yPosition = y;
        bodyEdge = new CircularBodyEdge(radius);
        this.sprite = sprite;
        this.radius = radius;
    }

    public void update(double elapsedSeconds) {
        if (!fixed) {
            body.update(elapsedSeconds);
        }
        if (sprite != null) {
            sprite.update(elapsedSeconds);
        }
    }

    public void draw(Graphics2D g) {
        if (sprite != null) {
            AffineTransform t = g.getTransform();
            g.translate(body.xPosition, body.yPosition);
            g.rotate(body.zRotation);
            sprite.draw(g, 0, 0);
            g.setTransform(t);
        }
    }

}
