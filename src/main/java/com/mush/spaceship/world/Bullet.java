/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.spaceship.world;

import com.mush.game.utils.physics.FreeBody;
import com.mush.spaceship.vehicle.VehicleGun;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author mush
 */
public class Bullet {

    public int ownerId;
    public boolean expired;
    public double age;
    public FreeBody body;
    public double maxAge;
    public double lastXPosition;
    public double lastYPosition;
    private double damage;

    public Bullet(FreeBody fromBody, VehicleGun fromGun) {
        expired = false;
        age = 0;
        damage = 1;
        body = new FreeBody(fromGun.bulletMass);
        body.xPosition = fromBody.xPosition + fromBody.getLocalToGlobalX(fromGun.x, fromGun.y);
        body.yPosition = fromBody.yPosition + fromBody.getLocalToGlobalY(fromGun.x, fromGun.y);
        body.setZRotation(fromBody.zRotation);
        body.xVelocity = fromBody.xVelocity;
        body.yVelocity = fromBody.yVelocity;
        lastXPosition = body.xPosition;
        lastYPosition = body.yPosition;
    }

    public void fire(double speed, double impact, double duration) {
        body.addLocalVelocity(0, -speed);
        this.maxAge = duration;
        this.age = 0;
        this.damage = impact;
    }

    public void update(double elapsedSeconds) {
        if (expired) {
            return;
        }
        age += elapsedSeconds;
        if (age > maxAge) {
            expired = true;
        } else {
            lastXPosition = body.xPosition;
            lastYPosition = body.yPosition;

            body.update(elapsedSeconds);
        }
    }

    public void draw(Graphics2D g) {
        if (expired) {
            return;
        }
        g.setColor(Color.WHITE);

//        g.drawOval((int) body.xPosition - 5, (int) body.yPosition - 5, 10, 10);
        g.drawLine((int) body.xPosition, (int) body.yPosition, (int) lastXPosition, (int) lastYPosition);
    }

    public double getDamage() {
        if (maxAge <= 0 || age > maxAge) {
            return 0;
        }
        double ageFactor = 1 - 0.5 * (age / maxAge);
        return damage * ageFactor;
    }

}
